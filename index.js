const express = require('express'),
    mysql = require('mysql2'),
    app = express(),
    cors = require('cors'),
    bodyParser = require('body-parser'),
    jsonRum = bodyParser.json({ type: 'application/json' }),
    history = require('connect-history-api-fallback');

app.use(cors());
app.use(history({ index: 'dist/index.html' }));

const connection = mysql.createConnection({
    host: "homelinkrumba.servebeer.com",
    user: "broccoli",
    database: "node",
    password: "teop6dwAYOU9gLpa",
    port: 3400
});

connection.connect(function (err) {
    if (err) {
        return console.error("Ошибка: " + err.message);
    }
    else {
        console.log("Подключение к серверу MySQL успешно установлено");
    }
});

app.get('/post', (req, res) => {
    connection.query('SELECT * FROM posts', (err, result) => {
        if (err) throw err;
        res.send(result);
    })
})

app.post('/add', jsonRum, (req, res) => {
    const data = {
        id: parseInt(req.body.id),
        title: req.body.title,
        img: req.body.img,
        small_body: req.body.small_body,
        body: req.body.body
    }
    //console.log(data)
    connection.query("INSERT INTO posts (id, title, img, small_body, body) VALUES (?, ?, ?, ?, ?)", [data.id, data.title, data.img, data.small_body, data.body], (err, result) => { if (err) throw err; res.send(result) })
})

app.listen(3000, () => { })